<?php

namespace App\Controller;
use App\Entity\Project;
use App\Entity\Techno;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class PortfolioController extends AbstractController
{
 /**
     * @Route("/", name="home")
     */
    public function home() {
        return $this->render('portfolio/home.html.twig', [ 'title' => 'Portfolio de Maxime', 'description' => 'Voir mes réalisations']);
    }

    /**
     * @Route("/portfolio", name="portfolio")
     */
    public function index()
    {
        $repo = $this->getDoctrine()->getRepository(Project::class);
        $projects = $repo->findAll();
        return $this->render('portfolio/index.html.twig', [
            'controller_name' => 'PortfolioController',
            'projects' => $projects
        ]);
    }
    /**
     * @Route("/portfolio/projectform", name="portfolio_projectform")
     */
    public function projectForm(Request $request, ObjectManager $manager) {
        $project = new Project();
        $form = $this->createFormBuilder($project)
        ->add('name')
        ->add('description')
        ->add('src')
        ->add ('save', SubmitType::class, [ 'label' => 'Ajouter'])
        ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($project);
            $manager->flush();
        }
        return $this->render('portfolio/projectform.html.twig', [ 'form' => $form->createVIew()]);
    }
        /**
     * @Route("/portfolio/technoform", name="portfolio_form")
     */
    public function technoForm(Request $request, ObjectManager $manager){
        $techno = new Techno();
        $form = $this->createFormBuilder($techno)
        ->add('name')
        ->add ('save', SubmitType::class, [ 'label' => 'Ajouter'])
        ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($techno);
            $manager->flush();
        }

        return $this->render('portfolio/technoform.html.twig', [ 'technoform' => $form->createVIew()]);
    }
}
