<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerA9TcnNQ\srcApp_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerA9TcnNQ/srcApp_KernelDevDebugContainer.php') {
    touch(__DIR__.'/ContainerA9TcnNQ.legacy');

    return;
}

if (!\class_exists(srcApp_KernelDevDebugContainer::class, false)) {
    \class_alias(\ContainerA9TcnNQ\srcApp_KernelDevDebugContainer::class, srcApp_KernelDevDebugContainer::class, false);
}

return new \ContainerA9TcnNQ\srcApp_KernelDevDebugContainer([
    'container.build_hash' => 'A9TcnNQ',
    'container.build_id' => '856749a3',
    'container.build_time' => 1559491883,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerA9TcnNQ');
